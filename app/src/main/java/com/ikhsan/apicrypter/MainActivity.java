package com.ikhsan.apicrypter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.net.URLDecoder;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView oriPlainView = (TextView) findViewById(R.id.ori_text);
        TextView chiperView = (TextView) findViewById(R.id.chiper_text);
        TextView plainView = (TextView) findViewById(R.id.plain_text);

        String ori_plain_text = "This is secret message!";
        ApiCrypter apiCrypter = new ApiCrypter("1234567887654321", "8765432112345678"); // Iv and key must be same as server
        String chiper_text = null;
        String plain_text = null;

        try {
            chiper_text = ApiCrypter.bytesToHex(apiCrypter.encrypt(ori_plain_text)); // Encrypted
            plain_text = URLDecoder.decode(new String(apiCrypter.decrypt(chiper_text), "UTF-8"), "UTF-8"); // Decrypted
        } catch (Exception e) {
            e.printStackTrace();
        }

        oriPlainView.setText(ori_plain_text);
        chiperView.setText(chiper_text);
        plainView.setText(plain_text);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
